Title: Sobre
Slug: about
Status: published

Olá, me chamo Lindemberg, trabalho como Desenvolvedor de Software a pouco mais de 6 anos, e venho me especializando em DevOps / SRE / Cloud desde então.
Sou formado no curso de Ciência da Computação e sempre busco aprender algo novo que me ajude no dia a dia.

No decorrer desses anos tive a experiência de trabalhar com várias linguagens dentre elas Python, Ruby, PHP, Javascript e Bash.
Também tive a oportunidade de atuar com Infra/Operações e me interessei bastante pelo estudo de servidores, containers, orquestração e Cloud.

Nas horas vagas costumo tocar guitarra, assistir uma série ou ler um bom livro.
