Title: Corrigindo erro de perda de conexão com dispositivos usb (Pop!_OS / Ubuntu)
Date: 2022-07-16 09:00
Modified: 2022-07-16 09:00
Category: Microcontrolers
Tags: python, arduino, iot, esp32, esp8266, microcontrolers
Slug: erro-usb-dialout
Authors: Berg Paulo
Status: published


Já faz algum tempo desde a última postagem nesse blog, muitas coisas aconteceram nesse último ano que acabaram tomando mais do meu tempo livre, e
acabei ficando sem postar as atualizações, tentarei manter a periodicidade.

Nesse final de semana, voltei a brincar um pouco com Micropython no meu ESP32, e atualmente estou utilizando Pop!_OS na versão 22.02 como sistema operacional.

Ao conectar o dispositivo para começar a realizar o upload de códigos Python usando Micropython, encontrei um probleminha bem chatinho, sempre que conectava o
notebook reconhecia o dispositivo normalmente como `/dev/ttyUSB0` por alguns segundos e depois perdia a conexão por algum motivo.

Depois de checar algumas coisas e consultar meu buscador favorito, consegui resolver esse problema e estou documentando aqui quais passos segui:

Como já havia dito por algum motivo o notebook perdia a conexão com o dispositivo, então a primeira coisa que verifiquei foi se meu ESP32 funciona
como esperado em outro computador para garantir que não seria algum problema no hardware ou algo do tipo.

Após o teste ser realizado, foi possível realizar o upload um código usando Arduino IDE em um computador com Windows instalado (Agradeço a minha noiva por ter cedido seu notebook para testes rsrsrs).

Segunda opção: Verificar se o dispositivo usb em si continuava sendo detectado pelo notebook, consegui confirmar isso com o comando `sudo lslsb`, o único momento em que o dispostivo não
era mais exibido era quando eu o desconectava do notebook.

Terceira opção: Checar os logs do kernel (`sudo dmesg -T`) pra conferir se tinha algo de útil que poderia aproveitar e bingo, nas últimas linhas exibindo que o dispositivo foi desconectado,
e um pouco antes disso, um programa chamado `BRLTTY` por algum motivo acabou interferindo de alguma forma na conexão com o ESP32.

Abaixo algumas linhas de log relevantes:

    :::bash
    $ sudo dmesg -T
    > [Sat Jul 16 09:29:37 2022] ch341 1-1.2:1.0: ch341-uart converter detected
    > [Sat Jul 16 09:29:37 2022] usb 1-1.2: ch341-uart converter now attached to ttyUSB0
    > [Sat Jul 16 09:29:38 2022] input: BRLTTY 6.4 Linux Screen Driver Keyboard as /devices/virtual/input/input136
    > [Sat Jul 16 09:29:38 2022] usb 1-1.2: usbfs: interface 0 claimed by ch341 while 'brltty' sets config #1
    > [Sat Jul 16 09:29:38 2022] ch341-uart ttyUSB0: ch341-uart converter now disconnected from ttyUSB0
    > [Sat Jul 16 09:29:38 2022] ch341 1-1.2:1.0: device disconnected
    > [Sat Jul 16 09:35:16 2022] usb 1-1.2: USB disconnect, device number 27

Pesquisando mais sobre esse programa e o erro relatado acima, encontrei alguns links de outros usuários que enfrentaram esse mesmo problema em distros baseadas no Ubuntu(como o Pop!_OS):
[Reddit](https://www.reddit.com/r/pop_os/comments/uf54bi/how_to_remove_or_disable_brltty/), [Stack Overflow](https://stackoverflow.com/questions/70123431/why-would-ch341-uart-is-disconnected-from-ttyusb).

Após a leitura de ambos os links citados acima, a solução de desabilitar o serviço funcionou bem, já que o pacote não pode ser removido diretamente, como podemos ver abaixo:

    ::bash
    $ sudo apt remove brltty -y
    Reading package lists... Done
    Building dependency tree... Done
    Reading state information... Done
    Some packages could not be installed. This may mean that you have
    requested an impossible situation or if you are using the unstable
    distribution that some required packages have not yet been created
    or been moved out of Incoming.
    The following information may help to resolve the situation:

    The following packages have unmet dependencies:
    pop-desktop : Depends: brltty but it is not going to be installed
                Recommends: io.elementary.sideload but it is not installable
                Recommends: pop-transition but it is not going to be installed
    E: Error, pkgProblemResolver::Resolve generated breaks, this may be caused by held packages.

Então o serviço foi desabilitado, para que não inicie automaticamente com os seguintes comandos:

    ::bash
    $ sudo systemctl stop brltty-udev.service
    $ sudo systemctl mask brltty-udev.service
    $ sudo systemctl stop brltty.service
    $ sudo systemctl disable brltty.service

Após essas configurações foi possível utilizar o dispositivo junto a Arduino IDE e Micropython sem mais problemas.