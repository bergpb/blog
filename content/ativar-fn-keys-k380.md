Title: Ativando as teclas F-keys por padrão no Logitech K380
Date: 2022-07-24 09:00
Modified: 2022-07-24 09:00
Category: Microcontrolers
Tags: teclado, logitech, k380
Slug: ativar-fkeys-k380
Authors: Berg Paulo
Status: published

A algum tempo realizei a compra de um novo teclado para utilizar no meu setup, na busca optei por algo simples, com suporte a conexão bluetooth
e principalmente que não ocupasse tanto espaço na mesa.

Depois de algumas pesquisas acabei optando pelo modelo [Logitech K380](https://www.logitech.com/pt-br/products/keyboards/k380-multi-device.920-007564.html), um modelo que funciona por bluetooth, com a facilidade de troca de conexão com até 3
dispositivos(ajuda bastante na troca entre o PC trabalho e pessoal) além da alta duração das pilhas(sim o mesmo utiliza duas pilhas do tipo AAA).

Depois de alguns dias de uso, uma coisa acabou me incomodando, pois era necessário o uso do fn para acionar as teclas F1, F2... como por exemplo para renomear um arquivo no VSCode era necessário apertar fn+F2.

Resolvi pesquisar um pouco se havia alguma forma de utilizar as teclas F-keys nesse modelo sem depender do fn, e acabei encontrando um projeto no github que poderia ajudar com isso:
[jergusg/k380-function-keys-conf](https://github.com/jergusg/k380-function-keys-conf).

Este script altera alguns arquivos de configuração no sistema para que as teclas F-keys sejam utilizadas por padrão, abaixo irei descrever os passos que utilizei, nada muito diferente do que está descrito no README.md do projeto:

Primeiro passo como na maioria dos casos, instalamos as dependências para executar o projeto:

    :::bash
    $ sudo apt install build-essential

Após isso iremos clonar o projeto e entrar na pasta do mesmo:

    :::bash
    $ git clone https://github.com/jergusg/k380-function-keys-conf && \
    cd jergusg/k380-function-keys-conf

Feito isso podemos executar o comando:

    :::bash
    $ make install

Agora precisamos descobrir o número do teclado na interface do tipo hidraw: `/dev/hidrawX` onde X pode ser `0, 1, 2, 3`, e isso pode ser feito com o seguinte comando:

    :::bash
    $ sudo ./fn_on.sh

Com essa informação em mãos agora podemos aplicar as configurações para o dispositivo(lembre-se on X pode ser 0, 1, 2, 3):

    :::bash
    sudo k380_conf -d /dev/hidrawX -f on

É possível trocar automaticamente para as teclas F-keys com o seguinte comando: `sudo make reload`, e após isso quando o dispositivo for reconectado o mesmo irá alterar automaticamente para o modo de uso das teclas F-keys.

Caso voçê deseje desativar essa função basta rodar o comando: `sudo k380_conf -d /dev/hidrawX -f off`, seguido do comando `sudo make reload` para que as aplicações sejam alteradas.
