Title: Primeiros passos com Grafana Loki na Raspberry Pi
Date: 2022-10-21 09:00
Category: Loki
Tags: grafana, loki, docker
Slug: grafana-loki-raspberrypi
Authors: Berg Paulo
Status: published


O __Loki__ tem como finalidade agregar logs, assim é possível direcionar os logs do docker para o __Loki__ e realizar queries para filtrar/buscar informações nos logs registrados.</br>
Mais informações na documentação oficial: [Grafana Loki](https://grafana.com/docs/loki/latest/installation/docker/)

Para começar a utilizar o __Loki__ é bem simples, foi necessário criar de configuração para ele, um para o __Promtail__, e instalar os drivers do __Loki__ para os nodes com Docker e um arquivo `.yml` para realizar o deploy da stack do __Loki__ junto ao cluster Swarm.

O primeiro passo foi logar em cada node do Swarm e instalar o driver do __Loki__:

    :::bash
    $ docker plugin install grafana/loki-docker-driver:latest --alias loki --grant-all-permissions

Mas não foi tão simples quanto parece, a instalação demorou um pouco mais do que o esperado, e ao ativar o driver o seguinte erro foi exibido:

    :::bash
    $ docker plugin enable loki
    error: dial unix /run/docker/plugins/199604ada86c8b346245f6328d19b49c10a9489660f4ffe26ad8babae3f54cd2/loki.sock: connect: no such file or directory

Pesquisando um pouco sobre esse erro, acabei encontrando uma issue no Github no próprio projeto do __Loki__ sobre, a recomendação é realizar o build do binário em Go do __Loki__ para dispositivos com arquitetura ARM.

Recomendo fazer os passos a seguir em um máquina com mais poder de processamento do que sua Raspberry Pi, esse processo demorou um pouco na minha máquina pessoal com I5 de 7 geração com 16Gb de Ram.

Abaixo os passos que segui para realizar o build do binário no Linux:</br >
OBS: Necessário ter o Go instalado, mais informações aqui: [How to Install Go on Linux](https://golangdocs.com/install-go-linux)

    :::bash
    $ git clone https://github.com/grafana/loki.git
    $ cd loki
    $ LOKI_VERSION=v2.6.1
    $ git checkout "tags/${LOKI_VERSION}" -b setup
    $ GOOS=linux GOARCH=arm GOARM=7 go build ./clients/cmd/docker-driver

Copiei o binário gerado para a Raspberry Pi,

    :::bash
    $ scp docker-driver pi@192.168.1.100:/tmp

Conectei via ssh com a Raspberry, e copiei o arquivo para a localização correta:

    :::bash
    $ sudo cp /tmp/docker-driver /var/lib/docker/plugins/199604ada86c8b346245f6328d19b49c10a9489660f4ffe26ad8babae3f54cd2/rootfs/bin

Após isso obtive sucesso ao ativar o plugin com o seguinte comando:

    :::bash
    $ docker plugin enable loki

Agora seguindo com a instalação do __Loki__ junto ao cluster Swarm, para isso realizarei o download dos arquivos de configuração recomendados na documentação oficial:

    :::bash
    $ wget https://raw.githubusercontent.com/grafana/loki/v2.6.1/cmd/loki/loki-local-config.yaml -O loki-config.yaml
    $ wget https://raw.githubusercontent.com/grafana/loki/v2.6.1/clients/cmd/promtail/promtail-docker-config.yaml -O promtail-config.yaml

Com os arquivos prontos, salvei eles junto as configurações do Swarm para que fosse possível utilizar na criação da stack mais a frente:

    :::bash
    $ docker config create loki_config loki-config.yml
    $ docker config create promtail_config promtail-config.yml

Criei um arquivo chamado `loki.yml` e inseri o seguinte conteúdo:

    :::yml
    version: "3.6"

    networks:
      grafana: {}

    volumes:
      grafana: {}
      loki-data: {}

    configs:
      loki_config:
        external: true
      promtail_config:
        external: true

    services:
      grafana:
        image: grafana/grafana:9.1.7
        user: "472"
        networks:
          - grafana
        volumes:
          - grafana:/var/lib/grafana
        ports:
          - "3000:3000"
        environment:
          - GF_SECURITY_ADMIN_USER=${ADMIN_USER:-admin}
          - GF_SECURITY_ADMIN_PASSWORD=${ADMIN_PASSWORD:-admin}
          - GF_USERS_ALLOW_SIGN_UP=false
        deploy:
          mode: replicated
          replicas: 1
        logging:
          driver: loki
          options:
            loki-url: "http://loki:3100/loki/api/v1/push"
            loki-retries: "5"
            loki-batch-size: "400"

      loki:
        image: grafana/loki:2.6.1
        command: -config.file=/etc/loki/loki-config.yaml
        networks:
          - grafana
        ports:
          - "3100:3100"
        volumes:
          - loki-data:/loki
        configs:
          - source: loki_config
            target: /etc/loki/loki-config.yaml
        deploy:
          mode: replicated
          replicas: 1

      promtail:
        image: grafana/promtail:2.6.1
        networks:
          - grafana
        volumes:
          - /var/log:/var/log
        command: -config.file=/etc/promtail/config.yml
        deploy:
          mode: replicated
          replicas: 1

Para realizar o deploy da stack executei o seguinte comando:

    :::bash
    $ docker stack deploy -c loki.yml loki

Com a stack disponível no cluster Swarm, foi possível acessar o Grafana através da http://192.168.1.100:3000, e configurar o __Loki__ para visulizar os logs do container do Grafana.

Para isso, acessei o Grafana > Configuration > Data Sources > Selecionar o __Loki__ > Em __Name__ adicionei __Loki__ e em __URL__ adicionei o nome do serviço do Swarm e porta (http://loki:3100) > Clique em Save & Test > Como o teste foi bem sucedido, a mensagem abaixo foi exibida:

![Configurando Loki no Grafana]({static}/images/posts/loki-raspberry/grafana_loki.png)

Após isso foi possível conferir os logs utilizando o __Loki__:

![Logs no Grafana Loki]({static}/images/posts/loki-raspberry/grafana_loki_2.png)

Referências:<br />
[Grafana Loki in Docker Swarm](https://medium.com/@mrschneider/grafana-loki-in-docker-swarm-78bfa6a761fa)<br />
[docker plugin enable Loki gives an error](https://github.com/grafana/loki/issues/974#issuecomment-840616055)
