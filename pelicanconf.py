# Theme-specific settings
SITENAME = 'Lindemberg Barbosa'
DOMAIN = 'bergpb.dev'
BIO_TEXT = 'Software Development | DevOps | SRE | IoT'
FOOTER_TEXT = 'Powered by <a target="_blank" href="https://getpelican.com">Pelican</a> | <a target="_blank" href="https://umami.bergpb.dev/share/QgVI76qd/Personal%20Blog">Analytics</a>'

DISQUS_SITENAME = "bergpb-blog"

SITE_AUTHOR = 'Berg'
TWITTER_USERNAME = '@lbergpb'
INDEX_DESCRIPTION = None

SIDEBAR_LINKS = [
    '<a href="/archive/">Posts</a>',
    '<a href="/tags/">Tags</a>',
    '<a href="/about/">Sobre</a>',
]

ICONS_PATH = 'images/icons'

SOCIAL_ICONS = [
    ('https://bolha.us/@bergpb', 'Mastodon', 'fa-brands fa-mastodon'),
    ('http://twitter.com/lbergpb', 'Twitter', 'fa-brands fa-twitter'),
    ('http://github.com/bergpb',   'GitHub',  'fa-brands fa-github'),
    ('http://gitlab.com/bergpb',   'GitLab',  'fa-brands fa-gitlab')
]

THEME_COLOR = '#FF8000'

DEFAULT_METADATA = { 'status': 'draft' }

# Pelican settings
RELATIVE_URLS = True
SITEURL = 'https://bergpb.dev'
TIMEZONE = 'America/Fortaleza'
DEFAULT_DATE = None
DEFAULT_DATE_FORMAT = '%B %-d, %Y'

THEME = 'theme'

ARTICLE_URL = '{date:%Y}/{date:%m}/{slug}/'
ARTICLES_HOME = 6
ARTICLE_SAVE_AS = ARTICLE_URL + 'index.html'

TAGS_URL = 'tags'
TAGS_SAVE_AS = 'tags/index.html'

PAGE_URL = '{slug}/'
PAGE_SAVE_AS = PAGE_URL + 'index.html'

ARCHIVES_SAVE_AS = 'archive/index.html'
YEAR_ARCHIVE_SAVE_AS = '{date:%Y}/index.html'
MONTH_ARCHIVE_SAVE_AS = '{date:%Y}/{date:%m}/index.html'

FEED_ATOM = 'atom.xml'
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None

TYPOGRIFY = True

MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.admonition': {},
        'markdown.extensions.codehilite': {'css_class': 'highlight'},
        'markdown.extensions.extra': {},
    },
    'output_format': 'html5',
}

CACHE_CONTENT = False
OUTPUT_PATH = 'public'
PATH = 'content'

templates = ['404.html']
TEMPLATE_PAGES = { page: page for page in templates }

STATIC_PATHS = ['images']

PLUGIN_PATHS = ['plugins']
PLUGINS = ['assets', 'neighbors', 'render_math', 'readtime']
ASSET_SOURCE_PATHS = ['static']
ASSET_CONFIG = [
    ('cache', False),
    ('manifest', False),
    ('url_expire', False),
    ('versions', False),
]

# Readtime plugin variables
READTIME_WPM = 230
READTIME_TEXT_SINGULAR = 'minute'
READTIME_TEXT_PLURAL = 'minutes'
