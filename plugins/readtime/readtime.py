import re
import math

from pelican import signals
from html.parser import HTMLParser


class MLStripper(HTMLParser):
    def __init__(self):
        super().__init__()
        self.reset()
        self.fed = []

    def handle_data(self, d):
        self.fed.append(d)

    def get_data(self):
        return ''.join(self.fed)


def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()


def calculate_readtime(content_object):
    wpm = content_object.settings.get('READTIME_WPM', 230)
    text_singular = content_object.settings.get('READTIME_TEXT_SINGULAR', 'minute')
    text_plural = content_object.settings.get('READTIME_TEXT_PLURAL', 'minutes')

    if content_object._content is not None:
        content = content_object._content

        text = strip_tags(content)
        words = re.split(r'[^0-9A-Za-z]+', text)

        text = text_plural
        num_words = len(words)
        minutes = int(math.ceil(num_words / wpm))

        if minutes <= 1:
            minutes = 1
            text = text_singular

        content_object.readtime = {
            "minutes": minutes,
            "text": text
        }


def register():
    signals.content_object_init.connect(calculate_readtime)
